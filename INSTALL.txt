This is a module that lets Drupal handle user- and session-management for phpBB 2.0.x, a popular forum / bulletin board system. The result is that users can be registered, maintained and login through Drupal. It does not integrate phpBB into a Drupal-page, phpBB has to be themed separately if you require this. This module requires mod_rewrite support on the server (or you have to modify phpBBs templates a lot).

The home of this module is: http://drupal.org/node/32818

Requirements:
PHP 4.x or 5.x
MySQL database
mod_rewrite support (aka "Clean URLs" in Drupal)
Drupal 4.7.x (You can use it with 4.6, but you have to make some changes to forms).
phpBB 2.0.18 - 2.0.21

If you are using this module already:
You need to enable user defined profiles, to avoid error messages, and upgrade to phpBB 2.0.21. Other than that, this should be a smooth transition. Simply upgrade Drupal and phpBB separately, then copy in the new module.

If you have a Drupal installation:
Existing users are migrated automatically the next time they log in. But you have to take over the user with id 2 since that userid overlaps with the default administrator of phpBB. Chances are you tested your site when you first installed, so you already own this user.

If you have a phpBB installation:
This is slightly more tricky, the module uses Drupal as the source and therefore has no facilities to get user-information from phpBB. I would be all ears if anyone wants to sponsor such a feature, but for the time being I recommend http://drupal.org/node/24637


Fresh installation instructions:
Please see: http://kepp.net/drupal/
